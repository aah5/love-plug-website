let width = Math.min(window.innerWidth,800)
let height = Math.max(width/2,400)
let annotating = false;

let mode = "edit" // "zoom", "edit", "play"

let opts = {
  title: "Pattern",
	cursor: {
		bind: {
			mousedown: (u, targ, handler) => {
				return e => {
					if (e.button == 0) {
						console.log(e)
						//handler(e);

						if (e.ctrlKey) {
							annotating = true;

							// switch select region to annotation color
							u.root.querySelector(".u-select").classList.add("u-annotate");
						}
					}
				}
			},
			mouseup: (self, targ, handler) => {
				return e => {
					if (e.button == 0) {
						if (annotating) {
							// tmp disable drag.setScale
							const _setScale = u.cursor.drag.setScale;
							u.cursor.drag.setScale = false;

							// fire original handler
						//	handler(e);

							// revert drag.setScale
							u.cursor.drag.setScale = _setScale;
						}
						else {
							//handler(e);
						}
					}
				};
			}
		},
	},
  id: "chart1",
  class: "my-chart",
  width: width,   // 800
  height: height, // 400
  scales: {
    "x": {
      time: false,
    }
  },
  series: [
    {
      label: "Time",
      value: (self, rawValue) => rawValue.toFixed(1) + "s"
    },
    {
      // initial toggled state (optional)
      show: true,

      spanGaps: false,

      // in-legend display
      label: "Level",
      value: (self, rawValue) => rawValue.toFixed(0),

      // series style
      stroke: "red",
      width: 1,
      fill: "rgba(255, 0, 0, 0.3)",
      dash: [10, 5],
    }
  ],
};

let data = [
  [0, 2, 10, 15, 30, 55, 70, 70, 80, 80, 120], // x-values (timestamps)
  [1, 4,  1,  7, 10, 11, 11,  2,  3, 11,  20]  // y-values (series 1)
];

let uplot = new uPlot(opts, data, document.getElementById("chart"));

