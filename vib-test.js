let vib_button = document.getElementById("vib-button")

vib_button.addEventListener("click", () => {
  vib_strength = document.getElementById("vib-strength").valueAsNumber
  
  window.postMessage({
    direction: "from-page-script",
    message: {command: "vibrate", strength: vib_strength, time: 60}
  }, "*")
})
